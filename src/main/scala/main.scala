import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import main.{b, d}


object ActorA {
  def apply(b: ActorRef): Props = Props(new ActorA(b))
}

class ActorA(b: ActorRef) extends Actor with ActorLogging {
  def receive: Receive = {
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }

  override def preStart(): Unit = {
    log.info(s"prestart de ${self.path.name}")
    b ! s"Hello ${b.path.name}"
  }
}

object ActorB {
  def apply(): Props = Props(new ActorB)
}

class ActorB extends Actor with ActorLogging {
  def receive: Receive = {
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
      sender ! s"Hello ${sender.path.name}"
  }
}

object ActorC {
  def apply(b: String): Props = Props(new ActorC(b))
}

class ActorC(b: String) extends Actor with ActorLogging {
  def receive: Receive = {
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }

  override def preStart(): Unit = {
    val acteurb = context.actorSelection("../"+b)
    acteurb ! "Salut b"
  }
}

object ActorD {
  case class Ecrisa(ar:ActorRef)
  case object Creer

  def apply() = Props(new ActorD)
}

class ActorD extends Actor with ActorLogging {
  import ActorD._

  def receive: Receive = {
    case Ecrisa(acteur) =>
      acteur ! "Bonjour"
    //      sender ! "ok je l'ai fait"
    case Creer =>
      context.actorOf(ActorA(self), name = "acteura")
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }
}



object ActorE {
  def apply(d: ActorRef,b: ActorRef): Props = Props(new ActorE(d,b))
}

class ActorE(d: ActorRef, b:ActorRef) extends Actor with ActorLogging {

  override def preStart(): Unit = {
    import ActorD._
    d ! Ecrisa(b)
    d ! Creer
  }

  def receive: Receive = {
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }
}

object main extends App {
  println("Hello")

  val systeme = ActorSystem("simplesys")
  val b = systeme.actorOf(ActorB(), "bob")
//  // a connaît b par son ActorRef
//  val a = systeme.actorOf(ActorA(b), name = "acteura")
//  // c connaît b par son nom (ActorPath)
//  val c = systeme.actorOf(ActorC("bob"), name = "acteurc")
//  // d entend parler de b par un message (du main)
  val d = systeme.actorOf(ActorD(), "acteurd")
//  import ActorD._
//  d ! Ecrisa(b) // il est dangereux d'envoyer des messages depuis un objet qui n'est pas un acteur
//  d ! Creer

  val e = systeme.actorOf(ActorE(d,b), name = "acteurE")
}